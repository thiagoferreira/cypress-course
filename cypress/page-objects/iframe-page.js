const element = {
    companyMenu: '#menu-main-menu-new .menu-item-has-children:nth-child(5)>a',
    careersLink: '#menu-header-menu .menu-item-has-children:nth-child(5) ul li:nth-child(2)>a',
    iframe: '#gnewtonIframe',
    testVacancyLink: '[ns-qa="Software Development Engineer in Test"]',
    applyPositionButton: '[ns-qa="applyBtn"]',
    noOptionQ1: '.gnewtonQuestions .gnewtonQuestionWrapper:nth-child(1) .gnewtonNo',
    noOptionQ2: '.gnewtonQuestions .gnewtonQuestionWrapper:nth-child(2) .gnewtonNo',
    continueButton: '[ns-qa="continueBtn"]',
    firstNameField: '[ns-qa="firstNameField"]',
    lastNameField: '[ns-qa="lastNameField"]',
    emailField: '[ns-qa="emailField"]',
    submitButton: '[ns-qa="submitBtn"]',
}

export class IframePage{
  
    goToCareersPage() {
        cy.get(element.companyMenu).click();
        cy.get(element.careersLink).click();
    }

    openTestVacancy() {
        cy.iframe(element.iframe)
        .find(element.testVacancyLink)
        .click()
    }

    clickApplyPositionBtn() {
        cy.iframe(element.iframe)
        .find(element.applyPositionButton)
        .click()
    }

    awnserNoForQuestions() {
        cy.iframe(element.iframe)
        .as('iframe')
        .find(element.noOptionQ1)
        .click()
        cy.get('@iframe')
        .find(element.noOptionQ2)
        .click()
    }

    clickContinueBtn() {
        cy.iframe(element.iframe)
        .find(element.continueButton)
        .click()
    }

    fillForm() {
        cy.iframe(element.iframe)
        .as('iframe')
        .find(element.firstNameField)
        .type('John')
        cy.get('@iframe')
        .find(element.lastNameField)
        .type('Woods')
        cy.get('@iframe')
        .find(element.emailField)
        .type('jw@testmail.com')
    }

    clickSubmitButton() {
        cy.iframe(element.iframe)
        .find(element.submitButton)
        .click()
    }

    validateRequiredField(field) {
        cy.iframe(element.iframe)
        .find(field)
        .invoke('prop', 'validationMessage')
        .should('equal', 'Please fill out this field.')
    }

    validateElementIsVisible(field) {
        cy.iframe(element.iframe)
        .find(field)
        .should('be.visible')
    }
}