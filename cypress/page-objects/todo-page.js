const element = {
    iputTextField: '.new-todo',
    listCompletedTodos: '.todo-list li.completed',
    listTodos: '.todo-list li'
}

export class TodoPage{

    todoLabel(todoIndex) {return `.todo-list li:nth-child(${todoIndex}) label`};
    checkbox(todoIndex) {return `.todo-list li:nth-child(${todoIndex}) .toggle`};

    navigate(){
        cy.visit('http://todomvc-app-for-testing.surge.sh/');
    }

    addTodo(text){
        // {enter} makes Cypress press Enter / Timeout to specific command
        //cy.get('.new-todo', {timeout: 5000}).type(text + '{enter}');
        cy.get(element.iputTextField).type(text + '{enter}');
    }

    validateTodoTxt(todoIndex, todoText){
        cy.get(this.todoLabel(todoIndex)).should('have.text', todoText);
    }

    validateToggleIsNotChecked(todoIndex){
        cy.get(this.checkbox(todoIndex)).should('not.be.checked');
    }

    markTodoAsCompleted(todoIndex){
        cy.get(this.checkbox(todoIndex)).click();
    }

    validateTodoHasCssLine(todoIndex){
        cy.get(this.todoLabel(todoIndex)).should('have.css', 'text-decoration-line', 'line-through');
    }

    clearCompletedTodos(){
        cy.contains('Clear completed').click();
    }

    validateNotExistCompletedTodos(){
        cy.get(element.listCompletedTodos).should('not.exist');
        // verify that doesn't have childs on teh list
        //cy.get('.todo-list').should('not.have.descendants', 'li');        
    }

    filterTodos(filter){
        cy.contains(filter).click();
    }

    validateNumberOfTodos(expectedNumber){
        cy.get(element.listTodos).should('have.length', expectedNumber); 
    }
}