/// <reference types="cypress" />
import { TodoPage } from "../page-objects/todo-page";
const todoPage = new TodoPage();

describe('todo actions', () =>{

    beforeEach(() =>{
        todoPage.navigate();
        todoPage.addTodo("New Cypress test");            
    });

    it('Should be able to add a new todo to the list ', () =>{
        todoPage.validateTodoTxt(1,"New Cypress test");
        todoPage.validateToggleIsNotChecked(1);
    });

    it('Should mark a todo as completed ', () =>{
        todoPage.markTodoAsCompleted(1);
        todoPage.validateTodoHasCssLine(1);
    });

    it('Should clear completed todos ', () =>{ 
        todoPage.markTodoAsCompleted(1);
        todoPage.clearCompletedTodos();
        todoPage.validateNotExistCompletedTodos();
    }); 

});