/// <reference types="cypress" />
import { TodoPage } from "../page-objects/todo-page";
const todoPage = new TodoPage();

describe('Visual Validation', () =>{
    before(() =>  todoPage.navigate());

    beforeEach(() =>
      cy.eyesOpen({
        appName: 'TAU TodoMVC',
        batchName: 'TAU TodoMVC',
        browser: [
          {name: 'chrome', width: 1024, height: 768},
          {name: 'chrome', width: 800, height: 600},
          {name: 'firefox', width: 1024, height: 768},
          {deviceName: 'iPhone X'},
        ]
      })
    );
  
    afterEach(() => cy.eyesClose());

    it('validate the visual when adding todos', () =>{
        cy.eyesCheckWindow('empty todo list');
        todoPage.addTodo('Clean the House');
        todoPage.addTodo('Learn JS');
        cy.eyesCheckWindow('two todos');
        todoPage.markTodoAsCompleted(1);
        cy.eyesCheckWindow('mark as completed');
    });

});