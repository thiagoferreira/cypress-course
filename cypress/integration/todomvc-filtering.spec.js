/// <reference types="cypress" />
import { TodoPage } from "../page-objects/todo-page";
const todoPage = new TodoPage();

describe('filtering actions', () =>{

    beforeEach(() =>{
        todoPage.navigate();
        todoPage.addTodo("New Cypress test"); 
        todoPage.addTodo("Refactoring"); 
        todoPage.addTodo("Code Review");
        todoPage.markTodoAsCompleted(2);           
    });   

    it('Should filter Active todos', () =>{
        todoPage.filterTodos('Active');
        todoPage.validateNumberOfTodos(2);
    });

    it('Should filter Completed todos', () =>{
        todoPage.filterTodos('Completed');
        todoPage.validateNumberOfTodos(1);  
    });

    it('Should filter All todos', () =>{
        todoPage.filterTodos('Completed');
        todoPage.filterTodos('All');
        todoPage.validateNumberOfTodos(3);
    });

});
