/// <reference types="cypress" />
import { IframePage } from "../page-objects/iframe-page";
const iframePage = new IframePage();

describe('iframe', () =>{

    it('Should interact with iframe', () =>{
        cy.visit('https://iodinesoftware.com/careers-2/');
        cy.viewport(1680, 1050);
        //iframePage.goToCareersPage();
        iframePage.openTestVacancy();
        cy.wait(500);
        iframePage.clickApplyPositionBtn(); 
        cy.wait(500);
        iframePage.awnserNoForQuestions();
        iframePage.clickContinueBtn();
        cy.wait(500)
        iframePage.clickContinueBtn();
        cy.wait(500)
        iframePage.clickContinueBtn();
        cy.wait(500)
        iframePage.clickContinueBtn();
        cy.wait(500)
        iframePage.clickContinueBtn();
        cy.wait(500)
        iframePage.fillForm();
        iframePage.clickSubmitButton();
        iframePage.validateRequiredField('#mobile');
        cy.wait(500)
        iframePage.validateElementIsVisible('#mobile');           
    });

});