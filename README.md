## Requirements
To run the tests you need to have installed on your machine:

- Node => 12
- Chrome
 - To run the visual test will be necessary have an API Key from https://eyes.applitools.com/
 - Also is necessary add the API key on the env variable for it you can run `set APPLITOOLS_API_KEY=YourKey` (for Mac and Linux is export instead set) if this command not add the env variable on your system you can follow the steps mentioned here: https://help.applitools.com/hc/en-us/articles/360006914732-The-runner-API-key

# How to Run the Automated Tests
On root folder of the project just run the command `npm i`

After the installation is finished run the command `npm test`

